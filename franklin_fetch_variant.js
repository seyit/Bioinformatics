var variants = ["1	1636391	1636391	C	A"]

variants.forEach( variant => {
  let v = variant.split('\t')
  let url = `https://franklin.genoox.com/api/snp/summary?chr=chr${v[0]}&pos=${v[1]}&ref=${v[3]}&alt=${v[4]}`
  setTimeout(() => {
    fetch(url).then(r => r.json().then(d => console.log(v[1], d.classification.classification,d.classification.rules_met,)).catch(function(error) {
        console.log(error);
    }))
  }, 500);
})
