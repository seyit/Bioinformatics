### BED Annotation

> Basic script for annotating BED file with RefSeq Exonic coordinates

Parse coordinates of genes and their exons within RefSeq and writes a BED file from this then runs Bedtools to annotate user's BED with annotation BED if prefered.

There are two files for reference, both given in the repository
* refseqhg19_extended: contains all the information for genes in RefSeq DB except the sequences.
* LRG_RefSeqGene.txt: contains reference transcript information for RefSeq Genes

Those reference files are up-to-date by February 2017. 

> Getting "exonic" coordinates: get_exon_annotation_file()

> Annotating BED files: annonate_my_bed()

### Usage
    get_exon_annotation_file(dumpfile='../Data/refseqhg19_extended', refseqfile='../Data/LRG_RefSeqGene.txt', outfile='../Data/annotations.bed')
    annonate_my_bed(yourbedfile='../Data/amplicons.bed',annotationsbed='../Data/annotations.bed')
        ! Second step requires [BEDtools](http://bedtools.readthedocs.io/en/latest/) in your system
