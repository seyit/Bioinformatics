import os

class refSeqGene:
    def __init__(self,taxid,geneid,symbol,reference):
        self.taxid = taxid
        self.geneid = geneid
        self.symbol = symbol
        self.reference = reference

def get_exon_annotation_file(dumpfile,refseqfile,outfile):

    def read_refseq_file(refseqfile):
        gene_list = {}
        fl = open(refseqfile,'r').read().split('\n')
        for row in fl[1:-1]:
            info = row.split('\t')
            if info[5] not in gene_list.keys():
                gene_list[info[5].split('.')[0]] = ""
            if info[9] == "reference standard":
                gene_list[info[5].split('.')[0]] = refSeqGene(info[0],info[1],info[2],info[5])
            else:
                continue
        return gene_list

    def parse_refseq_dump(dumpfile,refseqfile):
        parsed_dump = {}
        f = open(dumpfile,'r').read().split('\n')[1:]
        refs = read_refseq_file(refseqfile)
        for line in f:
            if len(line)>0:
                line=line.split("\t")
                if line[1] in refs.keys() and refs[line[1]] != '':
                    parsed_dump[line[1]] = [] # dictionary keys are hg19.refGene.names'
                    # first elements of 'value list' of keys are:
                    parsed_dump[line[1]].append({"chromosome":line[2],
                                                 "strand":line[3],
                                                 "txStart":line[4],
                                                 "txEnd": line[5],
                                                 "cdsStart":line[6],
                                                 "cdsEnd":line[7],
                                                 "exonCount":line[8],
                                                 "name2":line[12],
                                                 "cdsStat":(line[13],line[14])})

                    # second elements of 'value list' of keys are exon coordinates:
                    exonStarts = line[9].split(',')
                    exonEnds = line[10].split(',')

                    parsed_dump[line[1]].append({})
                    for i in range(len(exonStarts)):
                        if exonStarts[i] != '': #last coordinate may be empty
                            parsed_dump[line[1]][1]["exon"+str(i+1)] = (exonStarts[i],exonEnds[i])

        return parsed_dump

    x = parse_refseq_dump(dumpfile,refseqfile)

    annotations = open(outfile,'w')
    for ts,content in x.items():
        for exon,coords in content[1].items():
            annotations.write(content[0]['chromosome']+'\t'+
                              coords[0]+'\t'+
                              coords[1]+'\t'+
                              ts+'_'+content[0]['name2']+'_'+exon+'_'+content[0]['exonCount']+'\t'+
                              content[0]['strand']
                              +'\n')
    annotations.close()


def annonate_my_bed(yourbedfile, annotationsbed):
    # this is a basic os run, requires bedtools on your system.

    sortingbed = "sortBed -i {} > {}".format(annotationsbed,
                                             annotationsbed+'_sorted.bed')
    sortingannot = "sortBed -i {} > {}".format(yourbedfile,
                                               yourbedfile+'_sorted.bed')
    cmd = "bedtools closest  -a {} -b {} > {}".format(yourbedfile+'_sorted.bed',
                                                      annotationsbed+'_sorted.bed',
                                                      yourbedfile+'_annotated.bed')


    os.system(sortingannot)
    os.system(sortingbed)
    os.system(cmd)

