#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script allows you to batch download publications from PubMed.
It fetches publications and write them in to the file specified.

example usage at bottom
"""

__author__ = "Seyit Zor"

# {lib}
# requirements: biopython

from Bio import Entrez


# {code}

def search(query):
    Entrez.email = 'example@example.com'
    handle = Entrez.esearch(db='pubmed',
                            sort='relevance',
                            retmax='300',
                            retmode='xml',
                            term=query)
    results = Entrez.read(handle)
    return results


def fetch_details(id_list):
    ids = ','.join(id_list)
    Entrez.email = 'example@example.com'
    handle = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           id=ids)
    results = Entrez.read(handle)
    return results


def run(pmids, output_file_path):
    pmids = list(map(int, pmids))
    abstracts = {}
    for paper in fetch_details(list(map(str, pmids)))['PubmedArticle']:
        article = paper['MedlineCitation']['Article']
        title = article.get('ArticleTitle', 'NA')
        abstract = article.get('Abstract', {}).get('AbstractText', ['NA'])[0]
        pmid = paper['MedlineCitation']['PMID']
        abstracts[pmid] = [title, pmid, abstract]

    with open(output_file_path, 'w') as f:
        for i in pmids:
            f.write('@ ' + abstracts[str(i)][0] + '(' + str(abstracts[str(i)][1]) + ')' + '\n')
            f.write('\n')
            f.write(abstracts[str(i)][2] + '\n')
            f.write('\n')


# {example}

output_file_path = "<OUTOUT-FILE-PATH>"

pmids = [
    '15901906', '30884197', '20827233', '26667304',
    '28900629', '26951255', '22179739', '16951943'
]

run(pmids, output_file_path)
