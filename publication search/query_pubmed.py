#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script allows you to automate scientific literature query.
Plus adds fuzzy search partial_ratio as score to each publication
Output Options: ids, titles, abstracts, scores or both
"""

__author__ = "Seyit Zor"


# {lib} 
# requirements: biopython, fuzzywuzzy

from Bio import Entrez
from fuzzywuzzy import fuzz
import pprint


# {code}

def search(query, count):
    Entrez.email = 'example@example.com'
    handle = Entrez.esearch(db='pubmed',
                            sort='relevance',
                            retmax=str(count),
                            retmode='xml',
                            term=query)
    results = Entrez.read(handle)
    return results


def fetch_details(id_list):
    ids = ','.join(id_list)
    Entrez.email = 'example@example.com'
    handle = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           id=ids)
    results = Entrez.read(handle)
    return results


def run(search_terms, fuzzy_search_term, response_limit=100, print_ids=False, print_title=False, print_abstract=False, print_scores=True):
    ids = []
    articles = []
    sorted_papers = []

    for d in search_terms:
        results = search(d, response_limit)
        id_list = results['IdList']
        if len(id_list) > 0:
            papers = fetch_details(id_list)

            for i, paper in enumerate(papers['PubmedArticle']):
                article = paper['MedlineCitation']['Article']
                pmid = paper['MedlineCitation']['PMID']
                if pmid not in ids:
                    articles.append([pmid, article])

                ids.append(str(pmid))

    for article in articles:
        article_body = article[1]
        pmid = article[0]
        title = article_body.get('ArticleTitle', 'NA')
        if print_title:
            print(title)
        abstract = article_body.get('Abstract', {}).get('AbstractText', ['NA'])[0]
        if print_abstract:
            print(abstract)

        similarity = fuzz.partial_ratio(abstract, fuzzy_search_term)
        sorted_papers.append((similarity, str(pmid), title))

    if print_scores:
        pprint.pprint(sorted(sorted_papers, key=lambda x: -x[0]))

    if print_ids:
        pprint.pprint([i[1] for i in sorted(sorted_papers, key=lambda x: -x[0])])


# {example}

search_terms = ["ADA2 genetic correlation"]
fuzzy_search_term = "expanding spectrum genotype-phenotype correlation without negative"

run(search_terms, fuzzy_search_term, print_ids=False, print_title=False, print_abstract=False, print_scores=True)
