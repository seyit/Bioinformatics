
const fs = require("fs");
const endPoint = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi/";

var filePath = "/tmp/pubmed.txt";

var pubmedIdlist = [
    "25886721", "28639239",
    "30146570", "27623437",
    "21364753", "26542098",
    "29425688", "24281115",
    "18186271", "27721771",
];

function appendPublication(x) {
    delete x.uids
    let dataToWrite = Object.values(x)
    dataToWrite.forEach(function(d){
        fs.appendFile(filePath, JSON.stringify(d) + "\n", function (err) {
            if (err) throw err;
        });
    })
    console.log("Publications collected: " + (dataToWrite.length).toString());
}

function fetchFromAPI(data, callback) {
    fetch(endPoint, {
        method: "POST",
        body: data,
    })
        .then((r) => r.json())
        .then((result) => {
            callback(result.result);
        });
}

function collectPublications(pmidArray) {
    let data = new FormData();

    let count = 0;
    var limit = 3; // Maximum response limit of Entrez esummary API

    var length = pmidArray.length;
    var surplus = pmidArray.length % limit;

    fs.openSync(filePath, 'w')

    data.set("db", "pubmed");
    data.set("retmode", "json");

    if (length !== 0) {
        console.log("Collecting publications...");
        let collecting = setInterval(function () {
            if (count === length - surplus) {
                data.set("id", pmidArray.slice(length - surplus, length));
                fetchFromAPI(data, appendPublication)
                clearInterval(collecting);
            } else {
                data.set("id", pmidArray.slice(count, count + limit));
                fetchFromAPI(data, appendPublication)
            }
            count += limit
        }, 500);
    }
}

collectPublications(pubmedIdlist);
