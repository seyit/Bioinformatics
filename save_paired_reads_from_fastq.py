from Bio.SeqIO.QualityIO import FastqGeneralIterator

mixed_file = "/home/seyit/Desktop/BIOINFORMATICS/data/UR-CVD88-SD_S276_L001_R1_001.fastq"
paired_file = "/home/seyit/Desktop/BIOINFORMATICS/data/new.fastq"

out_handle = open(paired_file, "w")
prev = None
for curr in FastqGeneralIterator(open(mixed_file, "rU")):
    if curr[0].split()[0].endswith("/1"):
        prev = curr
    elif not curr[0].split()[0].endswith("/2"):
        raise ValueError("Expect IDs to end /1 and /2,\n%s" % curr[0])
    elif prev and prev[0].split()[0] == curr[0].split()[0][:-2] + "/1":
        out_handle.write("@%s\n%s\n+\n%s\n" % prev)
        out_handle.write("@%s\n%s\n+\n%s\n" % curr)
        prev = None
out_handle.close()
