import string
import glob
import os
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def convert_fastq(fname):
    b = open(fname,'r').read().split('\n')
    fq = fname.split('_',1)[0] + '_' +\
         fname.split('_',1)[-1:][0].split('-')[1].replace('_export.txt','.fastq')

    logger.info(" -- File is converting: %s -- " % (fq))

    f = open(fq,'w')
    beforecount,aftercount = 0,0
    for line in b:
        beforecount += 1
        line = line.split('\t')

        if len(line) >= 21:
            f.write('@' + line[0] + ':' +
                        str(line[1]) + ':*:' +
                        str(line[2]) + ':' +
                        str(line[3]) + ':' +
                        str(line[4]) + ':' +
                        str(line[5]) +
                        ' '+
                        str(line[7])+':'+
                        str(line[21])+':0:'+
                            line[6]+'\n')

            phred64 = """@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghi"""
            phred33 = """!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"""

            newq = line[9].translate(string.maketrans(phred64, phred33))

            f.write(line[8]+'\n'+'+'+'\n'+newq+'\n')
            aftercount += 1
        else:
            continue

    f.close()

    logger.info(" -- %s reads on raw file -- " % (beforecount))
    logger.info(" -- %s reads are edited -- " % (aftercount))

def runforest(folder_name):
    logger.info(" - Unzipping... - ")
    os.system('pigz -d '+folder_name+'/*.gz')

    logger.info(" - Converting... - ")
    for fl in glob.glob(folder_name+'/*_export.txt'):
        convert_fastq(fl)

    logger.info(" - Rezipping... - ")
    os.system('pigz '+folder_name+'/*.fastq')

# USAGE (iterates in directory and converts files with _export.txt extension)
# directory = "/"
# runforest(directory)