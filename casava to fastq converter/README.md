### Casava to Fastq Converter

> Basic script for converting Casava output (...export.txt) to regular Fastq

### Usage
    runforest(directory='../Data/Path') # Converts all Casava files in the directory you gave.
        ! Requires pigz installed in your machine
