
"""
The structure of ClinVar dump for the date of 10.04.2017
Recent ClinVar dump can be found on ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/

{'Guidelines': '',
  'Type': 'indel',
  'SubmitterCategories': '1',
  'RS# (dbSNP)': '397704705',
  'Cytogenetic': '7p22.1',
  'LastEvaluated': 'Jun 29, 2010',
  'ChromosomeAccession': 'NC_000007.13',
  'GeneSymbol': 'AP5Z1',
  'Stop': '4820847',
  'TestedInGTR': 'N',
  'PhenotypeList': 'Spastic paraplegia 48, autosomal recessive',
  'ClinSigSimple': '1',
  'PhenotypeIDS': 'MedGen:C3150901,OMIM:613647,Orphanet:ORPHA306511',
  'ClinicalSignificance': 'Pathogenic',
  'Chromosome': '7',
  'Origin': 'germline',
  'Name': 'NM_014855.2(AP5Z1):c.80_83delGGATinsTGCTGTAAACTGTAACTGTAAA (p.Arg27_Ala362delinsLeuLeuTer)',
  'RCVaccession': 'RCV000000012',
  'Assembly': 'GRCh37',
  'ReviewStatus': 'no assertion criteria provided',
  'OriginSimple': 'germline',
  'AlternateAllele': 'TGCTGTAAACTGTAACTGTAAA',
  'OtherIDs': 'OMIM Allelic Variant:613653.0001',
  'Start': '4820844',
  'GeneID': '9907',
  'HGNC_ID': 'HGNC:22197',
  'nsv/esv (dbVar)': '-',
  '#AlleleID': '15041',
  'ReferenceAllele': 'GGAT',
  'NumberSubmitters': '1'}

"""

def clinvar_dict(clinvar_dump_path:str) -> dict:
    """ Get ClinVar dump as Python Dictionary """

    with open(clinvar_dump_path,"r") as dump:
        content = dump.read().split('\n')
        header = content[0].split('\t')

        clinvar_dict = []
        for line in content[1:]:
            ln = line.split('\t')

            if len(ln) > 1:

                variant_dict = {header[i]: col for i, col in enumerate(ln)}

                rs = 'rs' + variant_dict['RS# (dbSNP)']
                if rs not in clinvar_dict.keys():
                    clinvar_dict[rs] = []

                clinvar_dict.append(variant_dict)

    return clinvar_dict

# clinvar_dict is not an efficient algorithm needs a lot of time to work

def clinvar_minimal_dict(clinvar_dump_path:str) -> dict:
    """ Get only pathogenicity records as Python Dictionary """

    relevant_columns = ['RS# (dbSNP)', 'ClinicalSignificance']

    with open(clinvar_dump_path,"r") as dump:
        content = dump.read().split('\n')
        header = content[0].split('\t')

        clinvar_dict = {}
        for line in content[1:]:
            ln = line.split('\t')
            if len(ln) > 1:

                variant_dict = {header[i]: col for i, col in enumerate(ln) if header[i] in relevant_columns}

                rs = 'rs' + variant_dict['RS# (dbSNP)']

                if rs not in clinvar_dict.keys():
                    clinvar_dict[rs] = []

                clinvar_dict[rs].append(variant_dict['ClinicalSignificance'].lower())

    return clinvar_dict

# clinvar_minimal_dict is more efficient than parsing all data,
# it only converts rs id's and clinical significances
