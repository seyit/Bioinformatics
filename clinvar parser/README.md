### ClinVar variant_summary Parser

> Basic script for converting ClinVar's tab delimited variant summary file to a Python Dictionary

The script is written in [Python](https://www.python.org/).

Sample data is given in the repository
* Data/clinvar_variant_summary_10042017.txt.zip (needs to be unzipped before use)


Those reference files are up-to-date by 10 April 2017.

> clinvar_dict() is not an efficient algorithm by the way it needs a lot of time to work

> clinvar_minimal_dict() is much more efficient than parsing all data it only get rs id's and clinical significances

### Usage
    clinvar_minimal_dict(filepath) #returns dictionary